---
title: "Deploy a Kubernetes Stateless App"
date: 2018-04-08T10:40:03+02:00
draft: true
tags: ["kubernetes", "docker", "containers"]
categories: ["Kubernetes"]

toc: true
---

*Please review [Kubernetes test instance](/post/kubernetes-test-instance/) if you need to setup a cluster first*

